﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Models
{
    public class SearchModel
    {
        public string stringSeaarch { get; set; }
        
        public List<string> Dates { get; set; }

        public List<string> Cities { get; set; }

        public List<string> Venues { get; set; }
    }
}
