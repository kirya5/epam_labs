﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Controllers
{
    public class RolesController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private UserManager<UserModel> _userManager;
        private IRoleService _roleService;

        public RolesController(
            IOptions<PathModel> options,
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _userManager = serviceFactory.CreateUserManager();
            _roleService = serviceFactory.CreateRoleService();
        }
        

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(Guid id)
        {
            UserModel user = await _userManager.FindByIdAsync(id.ToString());

            if (user != null)
            {
                var userRoles = _roleService.GetUserRoles(Guid.Parse(user.Id));
                var allRoles = _roleService.GetAllRoles();

                ChangeRoleModel model = new ChangeRoleModel
                {
                    UserId = user.Id,
                    Login = user.UserName,
                    UserRoles = userRoles.ToList(),
                    AllRoles = allRoles.ToList()
                };

                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, List<string> roles)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {

                _roleService.AddUserRole(id, roles);

                return RedirectToAction("Index", "User");
            }

            return NotFound();
        }
    }
}
