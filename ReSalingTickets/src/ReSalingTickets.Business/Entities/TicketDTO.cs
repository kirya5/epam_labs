﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class TicketDTO
    {
        public Guid Id { get; set; }
        public Guid EventId { get; set; }
        public decimal Price { get; set; } 
        public Guid SellerId { get; set; } 
        public bool Confirmation { get; set; }
    }
}