﻿using System;

namespace ReSalingTickets.Presentation.Models
{
    public class EventModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; } //Дата проведения
        public Guid VenueId { get; set; } //ID маста проведения
        public string PathToTheBanner { get; set; } //Путь к изображению  
        public string EventDescription { get; set; } //Описание
    }
}
