﻿using Microsoft.EntityFrameworkCore;
using ReSalingTickets.Data.Entities;

namespace ReSalingTickets.Data.ReSalingDataBaseContext
{
    public partial class ReSalingDbContext: DbContext
    {
        public ReSalingDbContext(DbContextOptions<ReSalingDbContext> options):base (options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRoles>()
                .HasKey(t => new { t.UserId, t.RoleId });

            modelBuilder.Entity<UserRoles>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.UserRoles)
                .HasForeignKey(pt => pt.RoleId);

            modelBuilder.Entity<UserRoles>()
                .HasOne(pt => pt.Role)
                .WithMany(t => t.UserRoles)
                .HasForeignKey(pt => pt.UserId);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Venue> Venues { get; set; }

        public DbSet<Role> Role { get; set; }
    }
}