﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReSalingTickets.Business.Autofac;
using ReSalingTickets.Data.Autofac;
using ReSalingTickets.Data.ReSalingDataBaseContext;
using ReSalingTickets.Presentation.AutofacModule;
using ReSalingTickets.Presentation.Configure;
using ReSalingTickets.Presentation.Filters;
using ReSalingTickets.Presentation.Infrastructure.Implementations;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.Services;
using System;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Net.Http.Headers;
using Sakura.AspNetCore.Mvc;

namespace ReSalingTickets.Presentation
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("mailclientsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<PathModel>(Configuration.GetSection("ConnectionString"));
            services.Configure<EmailServiceModel>(Configuration.GetSection("EmailService"));

            services.AddIdentity<UserModel, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 1;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                
            }).AddEntityFrameworkStores<ReSalingDbContext>();

            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ReSalingDbContext>(options =>
                options.UseSqlServer(connection));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            services.AddBootstrapPagerGenerator(options =>
            {
                options.ConfigureDefault();
            });

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                options.OutputFormatters.Add(new CsvSerializer());
                options.FormatterMappings.SetMediaTypeMappingForFormat("Csv", MediaTypeHeaderValue.Parse("application/csv"));
            }
            ).AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
             .AddDataAnnotationsLocalization();
            
            services.AddTransient<IUserStore<UserModel>, CustomUserStore>();
            services.AddTransient<IUserClaimStore<UserModel>, CustomUserStore>();
            services.AddTransient<IEmailSender, EmailService>();

            //Настраиваем Autofac
            var container = BuildContainer(services);

            return container.Resolve<IServiceProvider>();
        }

        private IContainer BuildContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterType<ServiceFactory>().As<IServiceFactory>();
            builder.RegisterType<ConnectionString>().As<IConnectionService>();
            builder.RegisterModule(new AutofacDataModule());
            builder.RegisterModule(new AutofacBusinessModule());
            builder.RegisterModule(new AutofacPresentationModule());
            builder.Populate(services);

            var container = builder.Build();
            return container;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var supportedCultures = new[]
                  {
                    new CultureInfo("br"),
                    new CultureInfo("en-GB")
                  };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("ru-RU"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });

            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "MyCookieAutorization",
                LoginPath = new PathString("/Account/Login/"),
                LogoutPath = new PathString("/Account/LogOff"),
                AccessDeniedPath = new PathString("/Account/Login/"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                ExpireTimeSpan = TimeSpan.FromDays(3)
                
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseIdentity();

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Api",
                    template: "{controller}/{action}/{id?}"
                    );
                routes.MapRoute(
                    name: "Databases",
                    template: "{baseName}/{controller}/{action}/{*id}",
                    defaults: new { baseName = "DataBase", Controller = "Home", action = "Index" }
                    );
            });
        }
        
    }
}
