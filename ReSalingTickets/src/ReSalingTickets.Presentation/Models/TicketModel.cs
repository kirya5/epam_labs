﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReSalingTickets.Presentation.Models
{
    public class TicketModel
    {
        public Guid Id { get; set; }
        [Display(Name ="Событие")]
        public Guid EventId { get; set; }
        public decimal Price { get; set; }
        public Guid SellerId { get; set; }
        public bool Confirmation { get; set; }
    }
}
