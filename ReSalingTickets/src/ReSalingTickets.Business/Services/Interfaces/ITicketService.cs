﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface ITicketService
    {
        IEnumerable<TicketDTO> GetAllTickets();
        IEnumerable<TicketShowDTO> GetAvilableTickets(Guid Eventid);
        void AddTicket(TicketDTO ticket, string userLogin);
        void DeleteTicket(Guid id);
        void UpdateTicket(TicketDTO ticket);
        TicketDTO GetTicket(Guid id);
        decimal GetPrice(Guid id);
        string GetEventName(Guid id);
        string GetSellerName(Guid id);
        string GetBuyerName(Guid id);
        IEnumerable<TicketShowDTO> GetMySellerTicket(string userName);
        IEnumerable<TicketShowDTO> GetMyBuyerTicket(string userName);
        IEnumerable<TicketShowDTO> GetTicketConfirmation(string userName);
        IEnumerable<TicketShowDTO> GetMySalesTicket(string userName);

    }
}
