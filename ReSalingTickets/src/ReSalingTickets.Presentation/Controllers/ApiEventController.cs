﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Implementations;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.ViewModels;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ReSalingTickets.Presentation.Controllers
{
    public class ApiEventController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private IEventService _eventService;
        private IVenueService _venueService;
        private ICityService _cityService;
        private readonly IMapper _mapper;

        public ApiEventController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _eventService = serviceFactory.CreateEventService();
            _venueService = serviceFactory.CreateVenueService();
            _cityService = serviceFactory.CreateCityService();
            _mapper = mapper;
        }

        // GET api/values/5
        [HttpPost]
        [Route("apievent/GetEvent/")]
        public IActionResult GetAllEvent([FromHeader]string orderSort, [FromHeader]string searchString, [FromHeader]int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(orderSort) ? "name" : "";
            ViewBag.CitySortParm = orderSort == "City" ? "city_desc" : "City";
            ViewBag.VenueSortParm = orderSort == "Venue" ? "venue_desc" : "Venue";
            ViewBag.VenueSortParm = orderSort == "Date" ? "date_desc" : "Date";

            var events = from e in _eventService.GetAllShowEvent()
                         select e;

            switch (orderSort)
            {
                case "name":
                    events = events.OrderByDescending(s => s.Name);
                    break;
                case "City":
                    events = events.OrderBy(s => s.CityName);
                    break;
                case "city_desc":
                    events = events.OrderByDescending(s => s.CityName);
                    break;
                case "Venue":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "venue_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                case "Date":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "date_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                default:
                    events = events.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return new SerializeResponceResult(events.ToPagedList(pageSize, pageNumber));
        }
        
        [HttpGet]
        [Route("apievent/GetCities/")]
        public IActionResult GetAllCities()
        {
            var cities = _cityService.GetAllСities();
            return new SerializeResponceResult(cities);
        }
       
        [HttpPost]
        [Route("apievent/GetVenues")]
        public IActionResult GetAllVenues([FromHeader][ModelBinder]List<string>cities)
        {
            var venues = _venueService.GetVenueForForm(cities);
            return new SerializeResponceResult(venues);
        }

        [HttpPost]
        [Route("apievent/GetDate")]
        public IActionResult GetAllDate([FromHeader][ModelBinder]List<string> venues)
        {
            var dateTimes = _eventService.GetTimeForForm(venues);
            
            return new SerializeResponceResult(dateTimes);
        }

        [HttpPost]
        [Route("apievent/Search")]
        public IActionResult Search([FromForm][ModelBinder]SearchModel model)
       {
            var result = _eventService.Search(_mapper.Map<SearchDTO>(model));

            if (result != null)
            {
                result = result.OrderBy(x => x.Count);
            }
                return new SerializeResponceResult(result);
        }
        
    }
}
