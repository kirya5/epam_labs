﻿using Autofac;
using ReSalingTickets.Data.Implements;
using ReSalingTickets.Data.Interfaces;

namespace ReSalingTickets.Data.Autofac
{
    public class AutofacDataModule:Module 
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TicketDbRepository>().As<IRepository>();

            base.Load(builder);
        }
    }
}