﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.ViewModels;
using Sakura.AspNetCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReSalingTickets.Presentation.Controllers
{
    public class EventController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private IEventService _eventService;
        private IVenueService _venueService;
        private readonly IMapper _mapper;

        public EventController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _mapper = mapper;
            _Resourcelocalizer = Resourcelocalizer;
            _eventService = serviceFactory.CreateEventService();
            _venueService = serviceFactory.CreateVenueService();
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddEvent()
        {
            var model = new AddEventViewModel
            {
                Venues = GetAllVenues()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AddEvent(AddEventViewModel eventModel, IFormFile file)
        {
            if (ModelState.IsValid && file != null && eventModel.Event.Date > DateTime.Now)
            {
                eventModel.Event.PathToTheBanner = ConvertToBase64(file);
                _eventService.AddEvent(_mapper.Map<Business.Entities.EventDTO>(eventModel.Event));
                return RedirectToAction("index", "Home");
            }
            else
            {
                var model = new AddEventViewModel
                {
                    Venues = GetAllVenues()
                };

                return View(model);
            }
        }

        [HttpGet]
        public IActionResult ShowAllEvents(string orderSort, string searchString, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(orderSort) ? "name" : "";
            ViewBag.CitySortParm = orderSort == "City" ? "city_desc" : "City";
            ViewBag.VenueSortParm = orderSort == "Venue" ? "venue_desc" : "Venue";
            ViewBag.VenueSortParm = orderSort == "Date" ? "date_desc" : "Date";

            var events = (from e in _eventService.GetAllShowEvent()
                         select e);
            switch (orderSort)
            {
                case "name":
                    events = events.OrderByDescending(s => s.Name);
                    break;
                case "City":
                    events = events.OrderBy(s => s.CityName);
                    break;
                case "city_desc":
                    events = events.OrderByDescending(s => s.CityName);
                    break;
                case "Venue":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "venue_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                case "Date":
                    events = events.OrderBy(s => s.VenueName);
                    break;
                case "date_desc":
                    events = events.OrderByDescending(s => s.VenueName);
                    break;
                default:
                    events = events.OrderBy(s => s.Name);
                    break;
            }

            var newItems = _mapper.Map<List<EventShowModel>>(events).AsQueryable();
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var sss = newItems.ToPagedList(pageSize, pageNumber);
            return View(sss);
        }

        public IActionResult ShowDetalsEvent(Guid id)
        {
            var item = GetAllEvent().Find(x => x.Id.Equals(id));

            return View(item);
        }

        public List<EventShowModel> GetAllEvent()
        {
            return _mapper.Map<List<EventShowModel>>(_eventService.GetAllShowEvent());
        }

        public IEnumerable<SelectListItem> GetAllVenues()
        {
            var items = _venueService.GetAllVenues();

            var venues = items
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });

            return venues;
        }

        private static string ConvertToBase64(IFormFile file)
        {
            if (file == null)
            {
                return "Изображение отсутствует";
            }
            using (var fileStream = file.OpenReadStream())
            using (var ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                var fileBytes = ms.ToArray();
                string s = Convert.ToBase64String(fileBytes);

                return s;
            }
        }
    }
}
