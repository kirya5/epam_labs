﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Controllers;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ReSalingTickets.Tests
{
    public class CityControllerTest
    {
        //[Fact]
        //public void IndexReturnsAViewResultWithAListOfCities()
        //{
        //    // Arrange
        //    var mock = new Mock<ICityService>();
        //    var mock1 = new Mock<IServiceFactory>();
        //    mock.Setup(x => x.GetAllСities()).Returns(GetTestCities());
            

        //    // Act
        //    var result = controller.ShowAllCities();
 
        //    // Assert
        //    var viewResult = Assert.IsType<ViewResult>(result);
        //    var model = Assert.IsAssignableFrom<IEnumerable<CityDTO>>(viewResult.Model);
        //    Assert.Equal(GetTestCities().Count, model.Count());
        //}

        private List<CityDTO> GetTestCities()
        {
            var phones = new List<CityDTO>
            {
                new CityDTO { Id=Guid.NewGuid(), Name="Gomel"},
                new CityDTO { Id=Guid.NewGuid(), Name="Minsk"},
                new CityDTO { Id=Guid.NewGuid(), Name="Moskva"},
                new CityDTO { Id=Guid.NewGuid(), Name="Grodno"},
            };

            return phones;
        }
    }
}
