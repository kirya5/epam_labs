﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Presentation.Models;

namespace ReSalingTickets.Presentation.PresentationMapperSettings
{
    public class PresentationMapperProfile : Profile
    {
        public PresentationMapperProfile()
        {
            CreateMap<CityModel, CityDTO>();
            CreateMap<CityDTO, CityModel>();

            CreateMap<EventModel, EventDTO>();
            CreateMap<EventDTO, EventModel>();

            CreateMap<EventShowModel, EventShowDTO>();
            CreateMap<EventShowDTO, EventShowModel>();

            CreateMap<TicketShowModel, TicketShowDTO>();
            CreateMap<TicketShowDTO, TicketShowModel>();

            CreateMap<TicketModel, TicketDTO>();
            CreateMap<TicketDTO, TicketModel>();

            CreateMap<UserModel, UserDTO>();
            CreateMap<UserDTO, UserModel>();

            CreateMap<VenueModel, VenueDTO>();
            CreateMap<VenueDTO, VenueModel>();

            CreateMap<OrderModel, OrderDTO>();
            CreateMap<OrderDTO, OrderModel>();

            CreateMap<SearchModel, SearchDTO>();
            CreateMap<SearchDTO, SearchModel>();
        }
    }
}