﻿namespace ReSalingTickets.Business.Entities
{
    public enum StatusOrderDTO
    {
        Sales,
        AwaitingApproval
    }
}
