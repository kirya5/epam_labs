﻿using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Data.Entities
{
    public class Role : IEntity
    {
        public Guid Id { get; set; }
        
        public Roles role { get; set; }

        public virtual ICollection<UserRoles> UserRoles { get; set; }
    }
}
