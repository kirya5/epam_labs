﻿using AutoMapper;
using ReSalingTickets.Business.Entities;

namespace ReSalingTickets.Business.BusinessMapperSettings
{
    public class BusinessMapperProfile : Profile
    {
        public BusinessMapperProfile()
        {
            CreateMap<CityDTO, Data.Entities.City>();
            CreateMap<Data.Entities.City, CityDTO>();

            CreateMap<EventDTO, Data.Entities.Event>();
            CreateMap<Data.Entities.Event, EventDTO>();

            CreateMap<OrderDTO, Data.Entities.Order>();
            CreateMap<Data.Entities.Order, OrderDTO>();

            CreateMap<TicketDTO, Data.Entities.Ticket>();
            CreateMap<Data.Entities.Ticket, TicketDTO>();

            CreateMap<UserDTO, Data.Entities.User>();
            CreateMap<Data.Entities.User, UserDTO>();

            CreateMap<VenueDTO, Data.Entities.Venue>();
            CreateMap<Data.Entities.Venue, VenueDTO>();
        }
    }
}