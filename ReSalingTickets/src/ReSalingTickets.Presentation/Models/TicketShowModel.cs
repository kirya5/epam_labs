﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReSalingTickets.Business.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReSalingTickets.Presentation.Models
{
    public class TicketShowModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Плакат")]
        public string PathToTheBanner { get; set; }
        
        [Display(Name = "Описание")]
        public string EventDescription { get; set; }

        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "ИмяПродавца")]
        public string NameSeller { get; set; }

        [Display(Name = "ИмяПокупателя")]
        public string NameBuyer { get; set; }

        public Guid NameSellerId { get; set; } //Имя продавца

        public Guid NameBuyerId { get; set; }
        
        [Display(Name = "Подтверждение")]
        public bool Confirmation { get; set; }

        [Display(Name = "Event")]
        public Guid TicketIventId { get; set; }
    }
}