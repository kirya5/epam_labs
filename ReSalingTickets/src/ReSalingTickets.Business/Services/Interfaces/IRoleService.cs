﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IRoleService
    {
        IEnumerable<string> GetUserRoles(Guid userId);
        void AddUserRole(Guid userId, List<string> roles);
        IEnumerable<string> GetAllRoles();
    }
}
