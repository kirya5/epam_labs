﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;

namespace ReSalingTickets.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;

        public HomeController(
            IServiceFactory serviceFactory,
            IStringLocalizer<HomeController> localizer,
            IStringLocalizer<SharedResource> Resourcelocalizer)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(PathModel model)
        {
            return RedirectToRoute("Databases", new { baseName = model.Type });
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
