﻿function GetAllEvent() {

    $.ajax({
        url: 'apievent/getallevent/' + stringResult,
        type: 'Get',
        data: JSON.stringify(venues),
        headers: {
            Accept: typeConnection,
            "Content-Type": "application/json"
        },
        dataType: types,
        success: function (data) {
            document.getElementById("selectDate").innerHTML = null;
            var z = document.getElementById("selectDate");
            z.innerHTML += "Дата проведения";
            for (var i = 0; i < data.length; i++) {
                var elem = document.createElement('li');
                var input = document.createElement('input');
                input.type = "checkbox";
                input.value = data[i];
                input.name = "Date";
                z.appendChild(elem);
                elem.appendChild(input);
                elem.innerHTML += data[i];

            }
        }
    });

}

function GetVenues() {

    var form = document.getElementById("searchForm");

    var cities = [];

    for (var i = 0; i < form.length; i++) {
        if (form[i].checked == true) {
            if (form[i].name == "City") {
                cities.push(form[i].value);
            }
        }
    }

    var typeConnection = form.type.value;
    var types = form.type.text;

    $.ajax({
        url: 'apievent/getallvenues/',
        type: 'POST',
        headers: {
            Accept: typeConnection,
            "Content-Type": "application/json"
        },
        dataType: types,
        data: JSON.stringify(cities),
        success: function (data) {
            document.getElementById("selectVenues").innerHTML = null;
            var z = document.getElementById("selectVenues");
            z.innerHTML += "Места проведения";
            for (var i = 0; i < data.length; i++) {
                var elem = document.createElement('li');
                var input = document.createElement('input');
                input.type = "checkbox";
                input.value = data[i];
                input.name = "Venue";
                z.appendChild(elem);
                elem.appendChild(input);
                elem.innerHTML += data[i];

            }
        }
    });
}

function GetDate() {

    var form = document.getElementById("searchForm");

    var venues = [];

    for (var i = 0; i < form.length; i++) {
        if (form[i].checked == true) {
            if (form[i].name == "Venue") {
                venues.push(form[i].value);
            }
        }
    }

    var typeConnection = form.type.value;
    var types = form.type.text;

    $.ajax({
        url: 'apievent/getalldate/',
        type: 'POST',
        data: JSON.stringify(venues),
        headers: {
            Accept: typeConnection,
            "Content-Type": "application/json"
        },
        dataType: types,
        success: function (data) {
            document.getElementById("selectDate").innerHTML = null;
            var z = document.getElementById("selectDate");
            z.innerHTML += "Дата проведения";
            for (var i = 0; i < data.length; i++) {
                var elem = document.createElement('li');
                var input = document.createElement('input');
                input.type = "checkbox";
                input.value = data[i];
                input.name = "Date";
                z.appendChild(elem);
                elem.appendChild(input);
                elem.innerHTML += data[i];

            }
        }
    });
}

function GetCities() {

    var xtml = new XMLHttpRequest();
    xtml.open('GET', 'apievent/getallcities', true);
    xtml.onreadystatechange = function () {
        if (xtml.readyState == 4 && xtml.status == 200) {
            hide();
            document.getElementById("select").innerHTML = null;
            var result = JSON.parse(xtml.responseText);
            var z = document.getElementById("select");
            z.innerHTML += "ГОРОДА";
            for (var i = 0; i < result.length; i++) {
                var elem = document.createElement('li');

                var input = document.createElement('input');
                input.type = "checkbox";
                input.value = result[i].Name;
                input.name = "City";
                z.appendChild(elem);
                elem.appendChild(input);
                elem.innerHTML += result[i].Name;
            }
        }
    };
    xtml.send(null);
    show();
}

$(document).ready(function () {
    GetCities();
});

function show() {
    document.getElementById("loader").style.display = "block";
}

function hide() {
    document.getElementById("loader").style.display = "none";
}

window.addEventListener("load", function () {

    var form = document.getElementById("searchForm");

    form.addEventListener("submit", function (e) {
        var model = {
            stringSeaarch: form.name.value
        }

        model.Cities = [];
        model.Venues = [];
        model.Dates = [];


        for (var i = 0; i < form.length; i++) {
            if (form[i].checked == true) {
                if (form[i].name == "City") {
                    model.Cities.push(form[i].value);
                }
                if (form[i].name == "Venue") {
                    model.Venues.push(form[i].value);
                }
                if (form[i].name == "Date") {
                    model.Dates.push(form[i].value);
                }
            }
        }

        var typeConnection = form.type.value;
        var types = form.type.text;

        $.ajax({
            url: 'apievent/search',
            type: 'POST',
            data: JSON.stringify(model),
            headers: {
                Accept: typeConnection,
                "Content-Type": "application/json"
            },
            dataType: types,
            success: function (data) {
                
            }
        });
    });
})