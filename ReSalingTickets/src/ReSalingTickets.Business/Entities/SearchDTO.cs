﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Entities
{
    public class SearchDTO
    {
        public string stringSeaarch { get; set; }

        public List<string> Dates { get; set; }

        public List<string> Cities { get; set; }

        public List<string> Venues { get; set; }
    }
}
