﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class VenueService : BaseService, IVenueService
    {
        public VenueService(IRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }

        public void AddVenue(VenueDTO venue)
        {
            if (venue != null)
            {
                venue.Id = Guid.NewGuid();
                _repository.Add(_mapper.Map<Data.Entities.Venue>(venue));
            }
        }

        public void DeleteVenue(Guid id) => _repository.Delete<Data.Entities.Venue>(id);

        public IEnumerable<VenueDTO> GetAllVenues()
        {
            var venues = _repository.GetAll<Data.Entities.Venue>();
            return _mapper.Map<List<VenueDTO>>(venues);
        }

        public string GetCityName(Guid cityId) => _repository.Get<Data.Entities.City>(cityId).Name;

        public string GetUserAdress(Guid userId) => _repository.Get<Data.Entities.User>(userId).Adress;

        public VenueDTO GetVenue(Guid id)=> _mapper.Map<VenueDTO>(_repository.Get<Data.Entities.Venue>(id));

        public IEnumerable<string> GetVenueForForm(List<string> items)
        {
            var venues = new List<string>();

            if (items != null)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var citiesId = _repository.GetAll<Data.Entities.City>().Where(x => x.Name.Equals(items[i])).Select(x => x.Id).ToList();

                    for (int j = 0; j < citiesId.Count(); j++)
                    {
                        venues.AddRange(_repository.GetAll<Data.Entities.Venue>().Where(x => x.CityId.Equals(citiesId[j])).Select(x => x.Name));
                    }
                }

                venues = venues.Distinct().ToList();

                return venues;
            }

            return null;
        }

        public void UpdateVenue(VenueDTO venue)
        {
            var item = _mapper.Map<Data.Entities.Venue>(venue);
            _repository.Update(item);
        }
    }
}
