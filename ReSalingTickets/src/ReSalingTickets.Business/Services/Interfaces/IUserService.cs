﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();

        void AddUser(UserDTO user);

        void DeleteUsers(Guid id);

        void UpdateUser(UserDTO user);

        UserDTO GetUser(Guid id);

        UserDTO GetUser(string login);

        string GetUserName(Guid id);

        string GetLocale(Guid id);
    }
}