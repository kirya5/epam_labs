﻿using ReSalingTickets.Business.Entities;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IStatusOrderService
    {
        IEnumerable<StatusOrderDTO> GetAllStatus();
    }
}
