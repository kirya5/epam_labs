﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReSalingTickets.Presentation.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ReSalingTickets.Presentation.Infrastructure.Implementations
{
    public enum ResponseType
    {
        Json,
        Xml,
        Csv
    }
    public class SerializeResponceResult : IActionResult
    {
        private static readonly Dictionary<string, ResponseType> Types = new Dictionary<string, ResponseType>()
        {
            { "application/json", ResponseType.Json },
            { "application/xml", ResponseType.Xml },
            { "application/csv", ResponseType.Csv }
        };

        public SerializeResponceResult(object data)
        {
            Data = data;
        }

        public SerializeResponceResult() { }

        public object Data { get; set; }

        public Task ExecuteResultAsync(ActionContext context)
        {

            var response = context.HttpContext.Response;

            var responseType = GetResponseTpe(context);

            response.ContentType = responseType;

            var contentType = GetResultType(responseType);

            switch (contentType)
            {
                case ResponseType.Json:
                    return SerializeToJson(response);

                case ResponseType.Xml:
                    return SerializeToXml(response);

                case ResponseType.Csv:
                    return SerializeToCSV(response);

                default:
                    return SerializeToJson(response);
            }
        }

        private string GetResponseTpe(ActionContext context)
        {
            var responseType = "application/json";

            var contentHeaders = context.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Accept")).Value;

            if (contentHeaders.Any())
            {
                responseType = contentHeaders;
            }

            return responseType;
        }

        private ResponseType GetResultType(string responseType)
        {
            return Types.FirstOrDefault(x => x.Key.Equals(responseType, StringComparison.CurrentCultureIgnoreCase)).Value;
        }

        private Task SerializeToJson(HttpResponse response)
        {
            var serializer = new JsonSerializer();
            //serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
            return Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(response.Body))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(sw, Data);
                }
            });
        }

        private Task SerializeToXml(HttpResponse response)
        {
            var xmlserializer = new XmlSerializer(Data.GetType());
            
            return Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(response.Body))
                using (XmlWriter writer = XmlWriter.Create(sw))
                {
                    xmlserializer.Serialize(writer, Data);
                }
            });
        }

        private Task SerializeToCSV(HttpResponse response)
        {
            var csvserializer = new CsvSerializer(Data.GetType());
            return Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(response.Body))
                {
                    csvserializer.Serialize(sw, Data);
                }
            });

        }
    }
}
