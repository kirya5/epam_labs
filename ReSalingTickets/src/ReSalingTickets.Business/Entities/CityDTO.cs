﻿using System;

namespace ReSalingTickets.Business.Entities
{
    public class CityDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}