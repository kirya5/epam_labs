﻿using System;
using System.Collections.Generic;

namespace ReSalingTickets.Data.Interfaces
{
    public interface IRepository
    {
        T Get<T>(Guid id) where T : class, IEntity;
        IEnumerable<T> GetAll<T>() where T : class, IEntity;
        void Add<T>(T item) where T : class, IEntity;
        void Add<T>(IEnumerable<T> items) where T : class, IEntity;
        void Delete<T>(Guid id) where T : class, IEntity;
        void Delete<T>(IEnumerable<T> items) where T : class, IEntity;
        void Update<T>(T item) where T : class, IEntity;
        //void SetInitialData();
    }
}
