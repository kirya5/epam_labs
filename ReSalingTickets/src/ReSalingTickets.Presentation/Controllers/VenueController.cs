﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.ViewModels;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ReSalingTickets.Presentation.Controllers
{
    public class VenueController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _resourcelocalizer;
        private IVenueService _venueRepository;
        private ICityService _cityRepository;
        private readonly IMapper _mapper;

        public VenueController(IMapper mapper,
             IServiceFactory serviceFactory,
             IStringLocalizer<SharedResource> resourcelocalizer)
        {
            _mapper = mapper;
            _resourcelocalizer = resourcelocalizer;
            _venueRepository = serviceFactory.CreateVenueService();
            _cityRepository = serviceFactory.CreateCityService();

        }
        // GET: /<controller>/
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddVenue()
        {
            var model = new AddVenuesViewModel
            {
                Cities = GetAllCities()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AddVenue (AddVenuesViewModel venueViewModel)
        {
            if (ModelState.IsValid)
            {
                _venueRepository.AddVenue(_mapper.Map<Business.Entities.VenueDTO>(venueViewModel.venueModel));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                    var model = new AddVenuesViewModel
                    {
                        Cities = GetAllCities(),
                        venueModel = venueViewModel.venueModel
                    };

                    return View(model);
            }
        }

        public IEnumerable<SelectListItem> GetAllCities()
        {
            var items = _cityRepository.GetAllСities();

            var cities = items
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });

            return cities;
        }
    }
}
