﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ReSalingTickets.Presentation.Controllers
{
    public class OrderController : Controller
    {
        // GET: /<controller>/
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private IOrderService _orderRepository;
        private IUserService _userService;
        private readonly IMapper _mapper;

        public OrderController(
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IMapper mapper)
        {
            _mapper = mapper;
            _orderRepository = serviceFactory.CreateOrderService();
            _userService = serviceFactory.CreateUserService();
            _Resourcelocalizer = Resourcelocalizer;
        }
        
        [Authorize]
        public IActionResult AddOrder(Guid id)
        {
            var userId = _userService.GetUser(User.Identity.Name).Id;

            var order = new OrderModel
            {
                BuyerId = userId,
                TicketId = id
            };

            _orderRepository.AddOrder(_mapper.Map<Business.Entities.OrderDTO>(order));

            return RedirectToAction("index", "Home");
        }
        
        [Authorize]
        public IActionResult EditStatus(Guid id)
        {
            _orderRepository.EditOrder(id);

            return RedirectToAction("index", "Home");
        }
    }
}
