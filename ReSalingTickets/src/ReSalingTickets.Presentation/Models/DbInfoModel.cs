﻿namespace ReSalingTickets.Presentation.Models
{
    public class DbInfoModel
    {
        public string Type { get; set; }

        public string Path { get; set; }
    }
}
