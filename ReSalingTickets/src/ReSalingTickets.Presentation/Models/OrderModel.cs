﻿using System;

namespace ReSalingTickets.Presentation.Models
{
    public class OrderModel
    {
        public Guid Id { get; set; }
        public Guid TicketId { get; set; }
        public string StatusOrder { get; set; } //статус заказа
        public Guid BuyerId { get; set; } //Имя покупателя
    }
}
