﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class Event:IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; } //Дата проведения
        public Guid VenueId { get; set; } //ID маста проведения
        public string PathToTheBanner { get; set; } //Путь к изображению    
        public string EventDascription { get; set; } //Описание
    }
}
