﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using ReSalingTickets.Presentation.Services;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ReSalingTickets.Presentation.Controllers
{
    public class AccountController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _Resourcelocalizer;
        private readonly IStringLocalizer<AccountController> _localizer;
        private IEmailSender _emailSender;
        private UserManager<UserModel> _userManager;


        public AccountController(
            IStringLocalizer<AccountController> localizer,
            IStringLocalizer<SharedResource> Resourcelocalizer,
            IServiceFactory serviceFactory,
            IEmailSender emailSender)
        {
            _Resourcelocalizer = Resourcelocalizer;
            _localizer = localizer;
            _emailSender = emailSender;
            _userManager = serviceFactory.CreateUserManager();
        }
        

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterUser item, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var existUser = await _userManager.FindByNameAsync(item.Login);
                
                if(existUser != null)
                {
                    Response.StatusCode = (int)HttpStatusCode.Conflict;
                    ModelState.AddModelError(string.Empty, _Resourcelocalizer["ЮзерУжеЕсть"]);
                    return View(item);
                }

                var user = new UserModel
                {
                    Email = item.Email,
                    Login = item.Login,
                    Password = item.Password,
                    Locale = "en-GB"
                };

                var result = await _userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    var newUser = await _userManager.FindByNameAsync(item.Login);
                    await CreateClaim(newUser);
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = newUser.Id, Email = newUser.Email },
                        protocol: HttpContext.Request.Scheme);
                        await _emailSender.SendEmailAsync(newUser.Email, "Confirm your account",
                        $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>Будте любезны подтвердить ))</a>");

                    return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    ModelState.AddModelError(string.Empty, _Resourcelocalizer["ОшибкаСервиса"]);
                    return View(item);
                }
            }

            Response.StatusCode = (int)HttpStatusCode.Conflict;
            return View(item);
        }

        [AllowAnonymous]
        public string Confirm(string Email)
        {
            return "На почтовый адрес " + Email + " Вам высланы дальнейшие" +
                    "инструкции по завершению регистрации";
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string Email)
        {
            UserModel user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                if (user.Email == Email)
                {
                    user.EmailConfirmed = true;
                    await _userManager.UpdateAsync(user);
                    return RedirectToAction("Index", "Home", new { ConfirmedEmail = user.Email });
                }
                else
                {
                    return RedirectToAction("Confirm", "Account", new { Email = user.Email });
                }
            }
            else
            {
                return RedirectToAction("Confirm", "Account", new { Email = "" });
            }
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new UserLogin());
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin item, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(item.Login);
                
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, _Resourcelocalizer["НетПользователя"]);
                }
                
                if (user != null)
                {
                    if (user.EmailConfirmed == false)
                    {
                        ModelState.AddModelError(string.Empty, "Вы не подтвердили свой email");
                        return View(item);
                    }

                    var password = new PasswordHasher<UserModel>().VerifyHashedPassword(user, user.PasswordHash, item.Password);

                    if (password == PasswordVerificationResult.Success)
                    {
                        await CreateClaim(user);

                        SetLanguage(user.Locale, Request.Path);
                        
                    }

                    if (!String.IsNullOrEmpty(item.ReturnUrl) && Url.IsLocalUrl(item.ReturnUrl))
                    {
                        return Redirect(item.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", _Resourcelocalizer["НеверныйЛогинИлиПароль"]);
                }
            }
            return View(item);
        }

        [HttpPost]
        public async Task<IActionResult> LogOf()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieAutorization");
            return RedirectToAction("Index", "Home");
        }

        
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
            CookieRequestCultureProvider.DefaultCookieName,
            CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
            new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) });
            
            return LocalRedirect(returnUrl);
        }

        private SelectList GetSelectListLocale<T>()
        {
            var selectListItems = Enum.GetValues(typeof(T)).Cast<T>()
            .ToDictionary(key => key, value => _localizer[value.ToString()]);
            var result = new SelectList(selectListItems, "Key", "Value");
            return result;
        }

        private async Task CreateClaim(UserModel user)
        {
            var Claims = await _userManager.GetClaimsAsync(user);
            Claims.Add(new Claim(ClaimTypes.Name, user.Login));

            var id = new ClaimsIdentity(Claims, "MyCookieAutorization", ClaimsIdentity.DefaultNameClaimType, ClaimTypes.Role);
            var principal = new ClaimsPrincipal(id);

            await HttpContext.Authentication.SignInAsync("MyCookieAutorization", principal);
        }
    }
}