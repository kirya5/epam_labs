﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using ReSalingTickets.Presentation.Infrastructure.Implementations;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Presentation.Configure
{
    public class ConnectionString : IConnectionService
    {
        private readonly CustomOptions _info;

        public ConnectionString(CustomOptions info)
        {
            _info = info;
        }

        public List<SelectListItem> GetDatabaseType()
        {
            return _info._connectionInfo.Select(x => new SelectListItem
            {
                Text = x.Key,
                Value = x.Key
            }).ToList();
        }

        public string GetPathConnections(string type)
        {
            return _info._connectionInfo.ToList().Find(x => x.Key.Equals(type)).Value;
        }

        public string GetTypeConnections(RouteData routeData)
        {
            return routeData.Values["baseName"].ToString();
        }
    }
}
