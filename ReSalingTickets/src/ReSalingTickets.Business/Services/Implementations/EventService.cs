﻿using AutoMapper;
using ReSalingTickets.Business.Entities;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReSalingTickets.Business.Services.Implementations
{
    public class EventService :BaseService, IEventService
    {
        public EventService(IRepository repository, IMapper mapper):base(repository, mapper)
        {
        }

        public void AddEvent(EventDTO Event)
        {
            if (Event != null)
            {
                Event.Id = Guid.NewGuid();
                _repository.Add(_mapper.Map<Data.Entities.Event>(Event));
            }
        }

        public void DeleteEvent(Guid id) => _repository.Delete<Data.Entities.Event>(id);

        public IEnumerable<EventDTO> GetAllEvent()
        {
            var events = _repository.GetAll<Data.Entities.Event>();
            if (events.Count() == 0)
            {
                return new List<EventDTO>();
            }
            return _mapper.Map<List<EventDTO>>(events);
        }

        public string GetCityName(Guid id) => _repository.Get<Data.Entities.City>(id).Name;

        public EventDTO GetEvent(Guid id)
        {
            var item = _repository.Get<Data.Entities.Event>(id);
            return _mapper.Map<EventDTO>(item);
        }

        public string GetVenueName(Guid id) => _repository.Get<Data.Entities.Venue>(id).Name;

        public void UpdateEvent(EventDTO events)
        {
            var item = _mapper.Map<Data.Entities.Event>(events);
            _repository.Update(item);
        }

        public IEnumerable<EventShowDTO> GetAllShowEvent()
        {
            var events = _repository.GetAll<Data.Entities.Event>();
            List<EventShowDTO> items = new List<EventShowDTO>();

            foreach(var r in events)
            {
                EventShowDTO item = new EventShowDTO();

                var venue = _repository.Get<Data.Entities.Venue>(r.VenueId);
                item.CityName = _repository.Get<Data.Entities.City>(venue.CityId).Name;
                item.VenueName = _repository.Get<Data.Entities.Venue>(r.VenueId).Name;
                item.Id = r.Id;
                item.PathToTheBanner = r.PathToTheBanner;
                item.Name = r.Name;
                item.Date = r.Date;

                items.Add(item);
            }

            return items;
        }

        public IEnumerable<string> GetTimeForForm(List<string> venues)
        {
            var dateTimes = new List<string>();
            if (venues != null)
            {
                for (int i = 0; i < venues.Count; i++)
                {
                    var venuesId = _repository.GetAll<Data.Entities.Venue>().Where(x => x.Name.Equals(venues[i])).Select(x => x.Id).ToList();
                    for (int j = 0; j < venuesId.Count(); j++)
                    {
                        dateTimes.AddRange(_repository.GetAll<Data.Entities.Event>().Where(x => x.VenueId.Equals(venuesId[j])).Select(x => x.Date.ToString()));
                    }
                }
            }

            dateTimes = dateTimes.Distinct().ToList();

            return dateTimes;
        }

        public IEnumerable<ShowApiEventDTO>Search(SearchDTO model)
        {
            if (model != null)
            {
                var eventApiShowModels = new List<ShowApiEventDTO>();
                var eventShowModels = new List<EventShowDTO>();
                bool isTested = false;
                int numberOfMatches = 0;

                if (model.Cities.Count != 0 || model.Venues.Count != 0 || model.Dates.Count != 0)
                {
                    eventShowModels = getEventsForSearch(model);
                }
                else
                {
                    eventShowModels = GetAllShowEvent().ToList();
                }
                
                    foreach (var t in eventShowModels)
                    {
                        ShowApiEventDTO newItem = new ShowApiEventDTO();

                    if (model.stringSeaarch != null)
                    {
                        string[] str1 = model.stringSeaarch.Split(' ');

                        for (int j = 0; j < str1.Length; j++)
                        {

                            if (t.Name.ToLower().Contains(str1[j].ToLower()))
                            {

                                if (!isTested)
                                {
                                    isTested = true;
                                    numberOfMatches++;
                                }

                                else
                                {
                                    numberOfMatches++;
                                }
                            }
                        }

                        newItem.Count = numberOfMatches;
                        newItem.eventModel = t;

                        if (numberOfMatches != 0)
                            eventApiShowModels.Add(newItem);

                        numberOfMatches = 0;
                    }
                    else
                    {
                        newItem.Count = 0;
                        newItem.eventModel = t;
                        eventApiShowModels.Add(newItem);
                    }
                    }

                    return eventApiShowModels;
            }

            return null;
        }

        public List<EventShowDTO> getEventsForSearch(SearchDTO model)
        {

            var cities = from c1 in model.Cities
                         join c2 in GetAllShowEvent()
                         on c1 equals c2.CityName
                         select _mapper.Map<EventShowDTO>(c2);
            
                var venues = from c1 in model.Venues
                             join c2 in cities
                             on c1 equals c2.Name
                             select c2;

                if (venues.Any())
                {
                    var Dates = from c in model.Dates
                                join c1 in venues
                                on DateTime.Parse(c) equals c1.Date
                                select c1;

                    if (Dates.Any())
                    {
                        Dates = Dates.Distinct();
                        return Dates.ToList();
                    }

                    venues = venues.Distinct();
                    return venues.ToList();
                }

                cities = cities.Distinct();
                return cities.ToList();
        }
    }
}
