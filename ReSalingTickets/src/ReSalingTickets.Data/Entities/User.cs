﻿using ReSalingTickets.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace ReSalingTickets.Data.Entities
{
    public class User:IEntity
    { 
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string PasswordHash { get; set; }
        public string Locale { get; set; }
        public string Adress { get; set; }
        public string NumberPhone { get; set; }
        public bool EmailConfirmed { get; set; }
        public virtual ICollection<UserRoles> UserRoles { get; set; }

    }
}
