﻿using ReSalingTickets.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReSalingTickets.Business.Services.Interfaces
{
    public interface IVenueService
    {
        IEnumerable<VenueDTO> GetAllVenues();
        void AddVenue(VenueDTO venue);
        void DeleteVenue(Guid id);
        void UpdateVenue(VenueDTO venue);
        VenueDTO GetVenue(Guid id);
        string GetCityName(Guid Cityid);
        string GetUserAdress(Guid Userid);
        IEnumerable<string> GetVenueForForm(List<string> items);
    }
}