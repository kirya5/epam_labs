﻿using ReSalingTickets.Data.Interfaces;
using System;

namespace ReSalingTickets.Data.Entities
{
    public class UserRoles
    {
        public Guid UserId { get; set; }

        public User User { get; set; }

        public Guid RoleId { get; set; }

        public Role Role { get; set; }
    }
}