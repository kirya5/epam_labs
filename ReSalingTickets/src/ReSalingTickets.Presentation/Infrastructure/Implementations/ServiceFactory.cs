﻿using Autofac;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using ReSalingTickets.Business.Services;
using ReSalingTickets.Business.Services.Interfaces;
using ReSalingTickets.Presentation.Configure;
using ReSalingTickets.Presentation.Infrastructure.Interfaces;
using ReSalingTickets.Presentation.Models;
using System;

namespace ReSalingTickets.Presentation.Infrastructure.Implementations
{
    public class ServiceFactory : IServiceFactory
    {
        private readonly Func<ICityService> _cityServiceBuilder;
        private readonly Func<IEventService> _eventServiceBuilder;
        private readonly Func<IOrderService> _orderServiceBuilder;
        private readonly Func<IRoleService> _roleServiceBuilder;
        private readonly Func<ITicketService> _ticketServiceBuilder;
        private readonly Func<IVenueService> _venueServiceBuilder;
        private readonly Func<IUserService> _userServiceBuilder;

        private readonly IOptions<PathModel> _options;
        private readonly IComponentContext _componentContext;

        public ServiceFactory(IOptions<PathModel> options, 
            Func<ICityService> cityServiceBuilder,
            Func<IVenueService> venueServiceBuilder,
            Func<IEventService> eventServiceBuilder,
            Func<IOrderService> orderServiceBuilder,
            Func<ITicketService> ticketServiceBuilder,
            Func<IRoleService> roleServiceBuilder,
            Func<IUserService> userServiceBuilder,
            IComponentContext context)
        {

            _componentContext = context;
            _options = options;

            _cityServiceBuilder = cityServiceBuilder;
            _venueServiceBuilder = venueServiceBuilder;
            _eventServiceBuilder = eventServiceBuilder;
            _orderServiceBuilder = orderServiceBuilder;
            _ticketServiceBuilder = ticketServiceBuilder;
            _roleServiceBuilder = roleServiceBuilder;
            _userServiceBuilder = userServiceBuilder;
        }

        public ICityService CreateCityService()
        {
            return _cityServiceBuilder();
        }

        public IConnectionService CreateConnectionService()
        {
            return new ConnectionString(new CustomOptions(_options));
        }

        public IEventService CreateEventService()
        {
            return _eventServiceBuilder();
        }

        public IOrderService CreateOrderService()
        {
            return _orderServiceBuilder();
        }

        public IRoleService CreateRoleService()
        {
            return _roleServiceBuilder();
        }

        public ITicketService CreateTicketService()
        {
            return _ticketServiceBuilder();
        }

        public UserManager<UserModel> CreateUserManager()
        {
            var _userService = CreateUserService();
            var _roleService = CreateRoleService();

            var userStore = _componentContext.Resolve<IUserStore<UserModel>>(
                new NamedParameter("userService", _userService),
                new NamedParameter("roleService", _roleService));

            return new UserManager<UserModel>(userStore, null, null, null, null, null, null, null, null);
        }

        public IUserService CreateUserService()
        {
            return _userServiceBuilder();
        }

        public IVenueService CreateVenueService()
        {
            return _venueServiceBuilder();
        }
    }
}